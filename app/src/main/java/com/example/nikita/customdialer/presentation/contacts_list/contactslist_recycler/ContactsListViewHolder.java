package com.example.nikita.customdialer.presentation.contacts_list.contactslist_recycler;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.nikita.customdialer.R;

public class ContactsListViewHolder extends RecyclerView.ViewHolder {
    final ImageView contactsAvatar;
    final TextView contactsName;
    final TextView contactsNumber;
    public ContactsListViewHolder(View view) {
        super(view);
        contactsAvatar = view.findViewById(R.id.iv_contacts_image);
        contactsName = view.findViewById(R.id.tv_contacts_name);
        contactsNumber = view.findViewById(R.id.tv_contacts_number);

    }
}
