package com.example.nikita.customdialer.presentation.dialer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.nikita.customdialer.R;

public class DialerFragment extends MvpAppCompatFragment implements View.OnClickListener, View.OnLongClickListener, DialerView {
    @InjectPresenter
    public DialerPresenter dialerPresenter;

    @ProvidePresenter
    public DialerPresenter providePresenter() {
        return DialerPresenter.getInstance();
    }

    private TextView inputNumber;

    public DialerFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialer, container, false);
        inputNumber = view.findViewById(R.id.tv_inputNumber);
        dialerPresenter = DialerPresenter.getInstance();
        initButtons(view);
        return view;
    }

    @Override
    public void setText(String text) {
        String originalText = (String) inputNumber.getText();
        String newText = originalText + text;
        inputNumber.setText(newText);
        dialerPresenter.updateContactList(newText);
    }

    @Override
    public void eraseText() {
        String originalText = (String) inputNumber.getText();
        if (originalText.length() > 0) {
            String newText = originalText.substring(0, originalText.length() - 1);
            inputNumber.setText(newText);
            dialerPresenter.updateContactList(newText);
        }
    }

    @Override
    public void onClick(View v) {
        dialerPresenter.buttonPressed(v.getId());
    }


    @Override
    public boolean onLongClick(View v) {
        dialerPresenter.zeroBtnLongPressed();
        return true;
    }

    private void initButtons(View view) {
        LinearLayout zeroButton = view.findViewById(R.id.button0);
        LinearLayout buttonOne = view.findViewById(R.id.button1);
        buttonOne.setOnClickListener(this);
        LinearLayout buttonTwo = view.findViewById(R.id.button2);
        buttonTwo.setOnClickListener(this);
        LinearLayout buttonThree = view.findViewById(R.id.button3);
        buttonThree.setOnClickListener(this);
        LinearLayout buttonFour = view.findViewById(R.id.button4);
        buttonFour.setOnClickListener(this);
        LinearLayout buttonFive = view.findViewById(R.id.button5);
        buttonFive.setOnClickListener(this);
        LinearLayout buttonSix = view.findViewById(R.id.button6);
        buttonSix.setOnClickListener(this);
        LinearLayout buttonSeven = view.findViewById(R.id.button7);
        buttonSeven.setOnClickListener(this);
        LinearLayout buttonEight = view.findViewById(R.id.button8);
        buttonEight.setOnClickListener(this);
        LinearLayout buttonNine = view.findViewById(R.id.button9);
        buttonNine.setOnClickListener(this);
        LinearLayout buttonStar = view.findViewById(R.id.button_star);
        buttonStar.setOnClickListener(this);
        LinearLayout buttonHash = view.findViewById(R.id.button_hash);
        buttonHash.setOnClickListener(this);
        LinearLayout buttonErase = view.findViewById(R.id.button_erase);
        buttonErase.setOnClickListener(this);
        zeroButton.setOnClickListener(this);
        zeroButton.setOnLongClickListener(this);
    }


}
