package com.example.nikita.customdialer.presentation.dialer;

import com.arellomobile.mvp.MvpView;

interface DialerView extends MvpView {
    void setText(String text);

    void eraseText();
}
