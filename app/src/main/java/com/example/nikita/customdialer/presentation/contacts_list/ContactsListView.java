package com.example.nikita.customdialer.presentation.contacts_list;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.nikita.customdialer.model.Contact;

import java.util.List;

interface ContactsListView extends MvpView {
    void loadContacts();

    void setListToAdapter(List<Contact> contacts);

    void setNewTextToAdapter(String newText);

    @StateStrategyType(SkipStrategy.class)
    void showProgressBar();

    @StateStrategyType(SkipStrategy.class)
    void hideProgressBar();

    void checkPermissions();

    void requestPermissionDialog();
}
