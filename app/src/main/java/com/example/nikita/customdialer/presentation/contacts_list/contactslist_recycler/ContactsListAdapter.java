package com.example.nikita.customdialer.presentation.contacts_list.contactslist_recycler;

import android.graphics.Color;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nikita.customdialer.R;
import com.example.nikita.customdialer.model.Contact;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListViewHolder> {
    private List<Contact> contactsList = new ArrayList<>();
    private List<Contact> originalList = new ArrayList<>();
    private static final String TAG = "ContactsListAdapter";
    private IsListEmpty isListEmpty;
    private String originalText = "";


    @NonNull
    @Override
    public ContactsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactsListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_entity, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsListViewHolder holder, int position) {
        Contact contact = contactsList.get(position);
        if (contact.getNumber().contains(originalText) && originalText.length() > 0) {
            holder.contactsNumber.setText(colorText(contact.getNumber().indexOf(originalText), contact.getNumber()));
            holder.contactsName.setText(contact.getName());
        } else if (originalText.length() > 0) {
            holder.contactsName.setText(colorText(contact.getNormalizedName().indexOf(originalText), contact.getName()));
            holder.contactsNumber.setText(contact.getNumber());
        } else {
            holder.contactsName.setText(contact.getName());
            holder.contactsNumber.setText(contact.getNumber());
        }
        Uri photo = contact.getPhotoUri();
        holder.contactsAvatar.setImageURI(photo);
        if (holder.contactsAvatar.getDrawable() == null)
            holder.contactsAvatar.setImageResource(R.drawable.ic_baseline_account_circle_24px);

    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

    public void setData(List<Contact> contacts) {
        originalList.clear();
        originalList.addAll(contacts);
        contactsList.clear();
        contactsList.addAll(contacts);
        notifyDataSetChanged();
        originalText = "";
        Log.d(TAG, "setData() called with: contacts = [" + contacts + "]");
    }

    private void onSuccess(List<Contact> contacts) {
        Log.d(TAG, "onSuccess() called with: contacts = [" + contacts.size() + "]");
        contactsList.clear();
        contactsList.addAll(contacts);
        isListEmpty.isListEmpty(contactsList.size() == 0);
        notifyDataSetChanged();

    }

    private void onError(Throwable throwable) {
        Log.d(TAG, "onError() called with: throwable = [" + throwable + "]");
    }

    private boolean filterContact(Contact contact, String newText) {
        Log.d(TAG, "filterContact() called with: contact = [" + contact.getName() + "]");
        return contact.getNormalizedName().contains(newText) || contact.getNumber().contains(newText);
    }

    public void updateListWithText(String newText) {
        originalText = newText;
        Log.d(TAG, "updateListWithText() called with: newText = [" + originalText + "]");
        Disposable subscribe = Observable.fromIterable(originalList)
                .filter(contact -> filterContact(contact, originalText))
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError);

    }


    private SpannableStringBuilder colorText(int startIndex, String text) {
        SpannableStringBuilder coloredText = new SpannableStringBuilder(text);
        coloredText.setSpan(
                new ForegroundColorSpan(Color.RED),
                startIndex,
                originalText.length() + startIndex,
                SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return coloredText;
    }

    public interface IsListEmpty {
        void isListEmpty(boolean isEmpty);
    }

    public void setIsListEmpty(IsListEmpty isListEmpty) {
        this.isListEmpty = isListEmpty;
    }
}
