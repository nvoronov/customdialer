package com.example.nikita.customdialer.presentation.dialer;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.nikita.customdialer.R;
import com.example.nikita.customdialer.presentation.contacts_list.ContactsListPresenter;

@InjectViewState
public class DialerPresenter extends MvpPresenter<DialerView> {
    private static DialerPresenter dialerPresenter = null;
    private ContactsListPresenter contactsListPresenter = ContactsListPresenter.getInstance();

    private DialerPresenter() {
    }

    public static DialerPresenter getInstance() {
        if (dialerPresenter == null)
            dialerPresenter = new DialerPresenter();
        return dialerPresenter;
    }

    public void buttonPressed(int viewID) {
        switch (viewID) {

            case R.id.button0:
                getViewState().setText("0");
                break;

            case R.id.button1:
                getViewState().setText("1");
                break;

            case R.id.button2:
                getViewState().setText("2");
                break;

            case R.id.button3:
                getViewState().setText("3");
                break;

            case R.id.button4:
                getViewState().setText("4");
                break;

            case R.id.button5:
                getViewState().setText("5");
                break;

            case R.id.button6:
                getViewState().setText("6");
                break;

            case R.id.button7:
                getViewState().setText("7");
                break;

            case R.id.button8:
                getViewState().setText("8");
                break;

            case R.id.button9:
                getViewState().setText("9");
                break;

            case R.id.button_star:
                getViewState().setText("*");
                break;

            case R.id.button_hash:
                getViewState().setText("#");
                break;

            case R.id.button_erase:
                getViewState().eraseText();
                break;


            default:
                break;
        }
    }

    public void zeroBtnLongPressed() {
        getViewState().setText("+");
    }

    public void updateContactList(String newText) {
        contactsListPresenter.updateContactList(newText);

    }
}
