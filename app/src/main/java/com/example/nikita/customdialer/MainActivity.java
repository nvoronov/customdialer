package com.example.nikita.customdialer;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.nikita.customdialer.presentation.contacts_list.ContactsListFragment;
import com.example.nikita.customdialer.presentation.dialer.DialerFragment;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frm_main_data, new ContactsListFragment());
        transaction.replace(R.id.frm_main_dialer, new DialerFragment());
        transaction.commit();


    }


}




