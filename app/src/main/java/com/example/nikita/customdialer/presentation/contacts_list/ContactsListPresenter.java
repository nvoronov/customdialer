package com.example.nikita.customdialer.presentation.contacts_list;

import android.content.Context;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.nikita.customdialer.interactor.ContactListInteractor;
import com.example.nikita.customdialer.interactor.ContactListInteractorImpl;
import com.example.nikita.customdialer.model.Contact;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class ContactsListPresenter extends MvpPresenter<ContactsListView> {

    private static ContactsListPresenter contactsListPresenter = null;
    private CompositeDisposable disposables = new CompositeDisposable();
    private ContactListInteractor contactListInteractor = ContactListInteractorImpl.getInstance();
    private static final String TAG = "ContactsListPresenter";

    private ContactsListPresenter() {
    }


    public static ContactsListPresenter getInstance() {
        if (contactsListPresenter == null) {
            contactsListPresenter = new ContactsListPresenter();
        }
        return contactsListPresenter;
    }

    @Override
    protected void onFirstViewAttach() {
        Log.d(TAG, "onFirstViewAttach() called");
        super.onFirstViewAttach();
        getViewState().checkPermissions();

    }

    void isPermissionGranted(boolean isGranted) {
        if (isGranted) {
            getViewState().loadContacts();
        } else
            getViewState().requestPermissionDialog();
    }


    void getContacts(Context context) {
        Log.d(TAG, "getContacts() called with: context = [" + context + "]");
        Disposable subscribe = Single.fromCallable(() -> contactListInteractor.getList(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getViewState().showProgressBar())
                .doFinally(() -> getViewState().hideProgressBar())
                .doOnError(throwable -> Log.e(TAG, "getContacts: ",throwable ))
                .subscribe(this::setContactsListFragment, throwable -> {
                    Log.e(TAG, "getContacts: ",throwable );
                });
        disposables.add(subscribe);

    }

    private void setContactsListFragment(List<Contact> contacts) {
        Log.d(TAG, "setContactsListFragment() called with: contacts = [" + contacts.size() + "]");
        getViewState().setListToAdapter(contacts);
    }

    public void updateContactList(String newText) {
        getViewState().setNewTextToAdapter(newText);
    }

    void disposeAll() {
        disposables.dispose();
    }
}
