package com.example.nikita.customdialer.model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.Switch;

import java.util.HashMap;
import java.util.Map;

public class Contact {
    private String name;
    private String number;
    private long id;
    private String normalizedName;
    private static Map<Character, String> lettersToDigits = fillMap();
    private Uri photoUri;



    public Uri getPhotoUri() {
        return photoUri;
    }


    public Contact(String name, String phoneNumber, Long id, Uri photoUri) {
        this.id = id;
        this.name = name;
        this.number = phoneNumber;
        this.normalizedName = normaliseName(name);
        this.photoUri = photoUri;
    }


    private String normaliseName(String name) {
        StringBuilder s = new StringBuilder();
        for (char c : name.toLowerCase().toCharArray()) {
            if (lettersToDigits.get(c) != null)
                s.append(lettersToDigits.get(c));
            else
                s.append(c);
        }
        return s.toString();
    }

    public String getNormalizedName() {
        return normalizedName;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public long getId() {
        return id;
    }

    private static Map<Character, String> fillMap() {
        Map<Character, String> newMap = new HashMap<>();
        for (char c : "абвгдеёжзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyz".toCharArray()) {
            newMap.computeIfAbsent(c, Contact::mapLetter);
        }
        return newMap;
    }

    private static String mapLetter(char c) {
        switch (c) {
            case 'а':
            case 'б':
            case 'в':
            case 'г':
            case 'a':
            case 'b':
            case 'c':
                return "2";
            case 'д':
            case 'е':
            case 'ё':
            case 'ж':
            case 'з':
            case 'd':
            case 'e':
            case 'f':
                return "3";
            case 'и':
            case 'й':
            case 'к':
            case 'л':
            case 'g':
            case 'h':
            case 'i':
                return "4";
            case 'м':
            case 'н':
            case 'о':
            case 'п':
            case 'j':
            case 'k':
            case 'l':
                return "5";
            case 'р':
            case 'с':
            case 'т':
            case 'у':
            case 'm':
            case 'n':
            case 'o':
                return "6";
            case 'ф':
            case 'х':
            case 'ц':
            case 'ч':
            case 'p':
            case 'q':
            case 'r':
            case 's':
                return "7";
            case 'ш':
            case 'щ':
            case 'ъ':
            case 'ы':
            case 't':
            case 'u':
            case 'v':
                return "8";
            case 'ь':
            case 'э':
            case 'ю':
            case 'я':
            case 'w':
            case 'x':
            case 'y':
            case 'z':
                return "9";

        }
        return null;
    }

}
