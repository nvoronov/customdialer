package com.example.nikita.customdialer.interactor;

import android.content.Context;
import android.util.Log;

import com.example.nikita.customdialer.model.Contact;
import com.example.nikita.customdialer.repository.ContactListRepository;

import java.util.List;

public class ContactListInteractorImpl implements ContactListInteractor {
    private ContactListRepository contactListRepository = ContactListRepository.getInstance();
    private static ContactListInteractorImpl contactListInteractor = null;
    private static final String TAG = "ContactListInteractorImpl";
    private ContactListInteractorImpl(){}

    public static ContactListInteractor getInstance() {
        if (contactListInteractor==null)
            contactListInteractor = new ContactListInteractorImpl();
        return contactListInteractor;
    }

    @Override
    public List<Contact> getList(Context context) {
        Log.d(TAG, "getList() in interactor called with: context = [" + context + "]");
        return contactListRepository.getList(context);
    }
}
