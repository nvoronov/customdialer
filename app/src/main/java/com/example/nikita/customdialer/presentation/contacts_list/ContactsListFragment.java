package com.example.nikita.customdialer.presentation.contacts_list;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.nikita.customdialer.R;
import com.example.nikita.customdialer.model.Contact;
import com.example.nikita.customdialer.presentation.contacts_list.contactslist_recycler.ContactsListAdapter;

import java.util.List;
import java.util.Objects;

public class ContactsListFragment extends MvpAppCompatFragment implements ContactsListView {

    @InjectPresenter
    public ContactsListPresenter contactsListPresenter;
    private RecyclerView recyclerView;
    private ContactsListAdapter contactsListAdapter;
    private TextView noContacts;
    private ProgressBar progressBar;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final String TAG = "ContactsListFragment";


    @ProvidePresenter
    ContactsListPresenter providePresenter() {
        return ContactsListPresenter.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.maindata, container, false);
        contactsListAdapter = new ContactsListAdapter();
        contactsListAdapter.setIsListEmpty(this::isListEmpty);
        recyclerView = view.findViewById(R.id.contacts_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(contactsListAdapter);
        noContacts = view.findViewById(R.id.tv_no_contacts);
        progressBar = view.findViewById(R.id.pb_contacts_list);
        return view;
    }

    @Override
    public void loadContacts() {
        if (contactsListAdapter.getItemCount() == 0){
            contactsListPresenter.getContacts(getContext());
        }
        Log.d(TAG, "loadContacts() called");
    }

    @Override
    public void setListToAdapter(List<Contact> contacts) {
        if (contacts.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            noContacts.setVisibility(View.INVISIBLE);
            contactsListAdapter.setData(contacts);
        }
        Log.d(TAG, "setListToAdapter() called with: contacts = [" + contacts + "]");
    }

    @Override
    public void setNewTextToAdapter(String newText) {
        contactsListAdapter.updateListWithText(newText);
        Log.d(TAG, "setNewTextToAdapter() called with: newText = [" + newText + "]");
    }

    @Override
    public void onStop() {
        super.onStop();
        contactsListPresenter.disposeAll();
    }

    @Override
    public void showProgressBar() {
        Log.d(TAG, "showProgressBar() called");
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        Log.d(TAG, "hideProgressBar() called");
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void checkPermissions() {
        contactsListPresenter.isPermissionGranted(ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);

    }

    @Override
    public void requestPermissionDialog() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()),
                Manifest.permission.READ_CONTACTS)) {

            new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                    .setTitle("Доступ к контактам")
                    .setMessage("Для отображения списка контактов выдайте разрешение")
                    .setNegativeButton("Exit", (dialog, which) -> System.exit(0))
                    .setPositiveButton("OK", (dialog, which) -> requestPermission())
                    .create()
                    .show();
        } else {
            requestPermission();
        }

    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                PERMISSIONS_REQUEST_READ_CONTACTS);
    }

    private void isListEmpty(boolean isEmpty) {
        if (isEmpty) {
            noContacts.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            noContacts.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts();
                Log.d(TAG, "loadContacts called from onRequestPermissions");
            } else {
                Toast.makeText(getContext(), "Разрешения не получены :(", Toast.LENGTH_LONG).show();
                System.exit(0);
            }
        }
    }


}
