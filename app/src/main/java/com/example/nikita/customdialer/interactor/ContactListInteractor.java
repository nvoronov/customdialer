package com.example.nikita.customdialer.interactor;

import android.content.Context;

import com.example.nikita.customdialer.model.Contact;

import java.util.List;

public interface ContactListInteractor {
    List<Contact> getList(Context context);
}
