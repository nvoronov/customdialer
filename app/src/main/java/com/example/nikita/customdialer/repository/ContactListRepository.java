package com.example.nikita.customdialer.repository;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.nikita.customdialer.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactListRepository {
    private ContentResolver resolver;
    private static volatile ContactListRepository contactListRepository = null;
    private List<Contact> cachedContacts = null;
    private static final String TAG = "ContactListRepository";

    private ContactListRepository() {
    }

    public static ContactListRepository getInstance() {
        if (contactListRepository == null) {
            synchronized (ContactListRepository.class) {
                if (contactListRepository == null) {
                    contactListRepository = new ContactListRepository();
                }
            }
        }
        return contactListRepository;
    }

    public List<Contact> getList(Context context) {
        if (cachedContacts == null) {

            resolver = context.getContentResolver();
            List<Contact> contacts = new ArrayList<>();
            String[] mProjection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Photo.PHOTO,
                    ContactsContract.CommonDataKinds.Phone.NUMBER
            };
            try (Cursor cursor = resolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    mProjection,
                    null,
                    null,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC"
            )) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {

                        String id = cursor.getString(cursor.getColumnIndex(mProjection[0]));
                        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
                        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

                        String name = cursor.getString(cursor.getColumnIndex(mProjection[1]));
                        Log.d(TAG, "getList: name" + name);
                        String phoneNumber = cleanNumber(cursor.getString(cursor.getColumnIndex(mProjection[3])));
                        Log.d(TAG, "getList: phoneNumber" + phoneNumber);
                        Contact contact = new Contact(name, phoneNumber, Long.valueOf(id), photoUri);
                        contacts.add(contact);
                    }
                    while (cursor.moveToNext());
                }

            }
            cachedContacts = new ArrayList<>();
            cachedContacts.addAll(contacts);
        }
        return cachedContacts;

    }

    private String cleanNumber(String number) {
        StringBuilder s = new StringBuilder();
        for (char c : number.toCharArray()) {
            if (!(c == '(' || c == ')' || c == '-' || c == ' ')) {
                s.append(c);
            }
        }
        return s.toString();
    }


}
